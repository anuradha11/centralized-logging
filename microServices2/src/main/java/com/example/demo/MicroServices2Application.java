package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroServices2Application {

	public static void main(String[] args) {
		SpringApplication.run(MicroServices2Application.class, args);
	}

}
